package com.geektechnique.transactions.dao;


import com.geektechnique.transactions.model.Employee;

public interface EmployeeDao {
	void insertEmployee(Employee cus);

	void deleteEmployeeById(String empid);
}