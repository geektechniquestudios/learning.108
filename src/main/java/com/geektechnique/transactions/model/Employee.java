package com.geektechnique.transactions.model;

import lombok.Data;

@Data
public class Employee {

	private String empId;
	private String empName;

}