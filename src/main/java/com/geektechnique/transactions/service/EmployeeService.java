package com.geektechnique.transactions.service;


import com.geektechnique.transactions.model.Employee;

public interface EmployeeService {
	void insertEmployee(Employee emp);

	void deleteEmployeeById(String empid);
}