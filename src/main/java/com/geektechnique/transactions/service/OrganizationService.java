package com.geektechnique.transactions.service;

import com.geektechnique.transactions.model.Employee;
import com.geektechnique.transactions.model.EmployeeHealthInsurance;

public interface  OrganizationService {

	public void joinOrganization(Employee employee, EmployeeHealthInsurance employeeHealthInsurance);

	public void leaveOrganization(Employee employee, EmployeeHealthInsurance employeeHealthInsurance);

}
